'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('apps','websiteurl'),
        queryInterface.removeColumn('apps','address'),
        queryInterface.addColumn('apps','domains',{
          type: Sequelize.ARRAY(Sequelize.STRING),
          allowNull: false
        })
      ])
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('apps','websiteurl', {
          type: Sequelize.STRING,
          allowNull: false
        }),
        queryInterface.addColumn('apps','address', {
          type: Sequelize.STRING,
          allowNull: false
        }),
        queryInterface.removeColumn('apps','domains')
      ])
    })
  }
};
