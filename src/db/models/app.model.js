const Sequelize = require('sequelize');
var uid = require('uid2');

const {Model} = Sequelize;

/**
 * @typedef {Object} AppPublicObject
 * @property {Number} id
 * @property {String} appname
 * @property {String} contactname
 * @property {String} email
 * @property {String} phone
 * @property {Array(String)} domains
 * @property {Number} registrar_id
 */

/**
 * @typedef {Class} AppModel
 * @property {Number} id
 * @property {String} appname
 * @property {String} contactname
 * @property {String} email
 * @property {String} phone
 * @property {Array(String)} domains
 * @property {Number} registrar_id
 * @property {String} app_secret
 */
class AppModel extends Model {
  /**
   * @returns {AppPublicObject}
   */
  getPublic() {
    return {
      id: this.id,
      appname: this.appname,
      contactname: this.contactname,
      email: this.email,
      phone: this.phone || '',
      domains: this.domains,
      registrar_id: this.registrar_id
    };
  }
}
const attributes = {
  appname: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false
  },
  contactname: {type: Sequelize.STRING},
  phone: {
    type: Sequelize.STRING,
    allowNull: true
  },
  domains: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    allowNull: false
  },
  registrar_id: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  app_secret: {
    type: Sequelize.STRING,
    unique: true,
    defaultValue: function() {
      return uid(42);
    }
  }
};

module.exports = {
  init: (sequelize) => {
    AppModel.init(attributes, {
      sequelize,
      modelName: 'apps'
    });
  },
  associate: (models) => {
    AppModel.belongsTo(models.User.model, {foreignKey : 'registrar_id', targetKey: 'id', as: 'registrar'});
  },
  get model() {
    return AppModel;
  }
};
