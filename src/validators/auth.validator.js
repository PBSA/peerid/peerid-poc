const Joi = require('joi');
const {Op} = require('sequelize');
const BaseValidator = require('./abstract/base.validator');
const ValidateError = require('../errors/validate.error');

class AuthValidator extends BaseValidator {
  /**
   * @param {AppRepository} opts.appRepository
   * @param {AccessTokenRepository} opts.accessTokenRepository
   * @param {GrantCodeRepository} opts.grantCodeRepository
   */
  constructor(opts) {
    super();

    this.appRepository = opts.appRepository;
    this.accessTokenRepository = opts.accessTokenRepository;
    this.grantCodeRepository = opts.grantCodeRepository;

    this.loggedOnly = this.loggedOnly.bind(this);
    this.validatePeerplaysLogin = this.validatePeerplaysLogin.bind(this);
    this.validateCode = this.validateCode.bind(this);
    this.validateAccessToken = this.validateAccessToken.bind(this);
  }

  loggedOnly() {
    return this.validate(null, null, async (req) => {

      if (!req.isAuthenticated()) {
        throw new ValidateError(401, 'unauthorized');
      }

      return null;
    });
  }

  validatePeerplaysLogin() {
    const bodySchema = {
      login: Joi.string().required(),
      password: Joi.string().required(),
      appId: Joi.number().integer().optional()
    };
    
    return this.validate(null, bodySchema, (req, query, body) => body);
  }

  validateCode() {
    const bodySchema = {
      code: Joi.string().required(),
      client_id: Joi.number().integer().required(),
      client_secret: Joi.string().required()
    };

    return this.validate(null, bodySchema, async(req, query, body) => {
      const {code, client_id, client_secret} = body;

      const AppExists = await this.appRepository.model.findOne({where:
      {
        id: client_id,
        app_secret: client_secret
      }});

      if(!AppExists) {
        throw new ValidateError(400, 'Validate error', {
          client_id: 'client_id or client_secret invalid'
        });
      }

      const GrantCodeExists = await this.grantCodeRepository.model.findOne({where:{code}});

      if(!GrantCodeExists) {
        throw new ValidateError(400, 'Validate error', {
          code: 'invalid code'
        });
      }

      if(GrantCodeExists.app_id !== client_id) {
        throw new ValidateError(400, 'Validate error', {
          code: 'code doesn\'t belong to this app'
        });
      }

      return {
        grantCodeId: GrantCodeExists.id,
        appId: AppExists.id,
        scope: GrantCodeExists.scope
      };
    });
  }

  validateAccessToken() {
    return this.validate(null, null, async (req) => {

      if(!req.headers['Authorization'] || !req.headers['ClientID']) {
        throw new ValidateError(401, 'unauthorized');
      }

      const authHeaderArr = req.headers['Authorization'].split(' ');

      if(authHeaderArr.length !== 2 && authHeaderArr[0] !== 'Bearer') {
        throw new ValidateError(401, 'Authorization invalid');
      }

      const AccessToken = await this.accessTokenRepository.findOne({
        where: {
          app_id: req.headers['ClientID'],
          token: authHeaderArr[1],
          expires: {
            [Op.gte]: new Date()
          }
        }
      });

      if(!AccessToken) {
        throw new ValidateError(401, 'Access Token invalid or expired');
      }

      return null;
    });
  }
}

module.exports = AuthValidator;
