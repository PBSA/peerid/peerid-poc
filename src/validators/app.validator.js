const Joi = require('joi');
const BaseValidator = require('./abstract/base.validator');
const ValidateError = require('../errors/validate.error');

class AuthValidator extends BaseValidator {
  /**
   * @param {AppRepository} opts.appRepository
   */
  constructor(opts) {
    super();

    this.appRepository = opts.appRepository;

    this.registerApp = this.registerApp.bind(this);
    this.deleteApp = this.deleteApp.bind(this);
    this.validateTransaction = this.validateTransaction.bind(this);
  }

  registerApp() {
    const bodySchema = {
      appname: Joi.string().min(3).max(255).required(),
      email: Joi.string().email().required(),
      contactname: Joi.string().min(2).max(255).required(),
      phone: Joi.string().regex(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/).required(),
      domains: Joi.array(Joi.string().uri()).required(),
      operations: Joi.array().items(Joi.number().integer().min(0).max(90)).required()
    };
    
    return this.validate(null, bodySchema, async (req, query, body) => {
      const {appname} = body;
      const AppNameExists = await this.appRepository.model.findOne({
        where: {
          appname
        }
      });

      if(AppNameExists) {
        return ValidateError(400, 'Validate error', {
          appname: 'Appname already exits'
        });
      }

      return body;
    });
  }

  deleteApp() {
    const querySchema = {
      id: Joi.number().integer().required()
    };

    return this.validate(querySchema, null, async (req, query) => {
      const {id} = query;

      const AppExists = await this.appRepository.model.findOne({
        where: {
          id
        }
      });

      if(!AppExists) {
        return ValidateError(400, 'Validate error', {
          id: 'App does not exist'
        });
      }

      return AppExists;
    });
  }

  validateTransaction() {
    const bodySchema = {
      transaction: Joi.object().keys({
        ref_block_num: Joi.number().required(),
        ref_block_prefix: Joi.number().required(),
        expiration: Joi.string().required(),
        operations: Joi.array().min(1).items(Joi.array().length(2).items(Joi.number().integer(), Joi.object())).required(),
        extensions: Joi.array().optional(),
        signatures: Joi.array().required()
      }).required()
    };

    return this.validate(null, bodySchema, async (req, query, body) => {
      const {transaction} = body;

      const authHeader = req.headers['Authorization'].split(' ')[1];
      const AccessToken = await this.accessTokenRepository.findOne({
        where: {
          token: authHeader
        }
      });

      const Ops = await this.operationRepository.findAll({
        where: {app_id: AccessToken.app_id}
      });
  
      const OpsArr = Ops.map(({operation_requested}) => operation_requested);

      const allOpsExist = transaction.operations.every((op) => OpsArr.indexOf(op[0]) >= 0);

      if(!allOpsExist) {
        throw new ValidateError('403', 'operations invalid');
      }

      return transaction;
    });
  }
}

module.exports = AuthValidator;