class UsersController {

  /**
   * @param {AuthValidator} opts.authValidator
   * @param {UserValidator} opts.userValidator
   * @param {UserService} opts.userService
   */
  constructor(opts) {
    this.authValidator = opts.authValidator;
    this.userService = opts.userService;
    this.userValidator = opts.userValidator;
  }

  /**
   * Array of routes processed by this controller
   * @returns {*[]}
   */
  getRoutes() {
    return [
      [
        'get', '/api/v1/users',
        this.authValidator.loggedOnly,
        this.userValidator.getUsers,
        this.getUsers.bind(this)
      ]
    ];
  }

  async getUsers(user, {search, limit, skip}) {
    skip = skip || 0;
    return await this.userService.searchUsers(search, limit, skip);
  }

}

module.exports = UsersController;
