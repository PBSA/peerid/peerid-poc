const chai = require('chai');
const chaiHttp = require('./helpers/node_modules/chai-http');

chai.use(chaiHttp);

const describe = global.describe;

describe('REST API', () => {
  describe('auth.controller', () => {
    require('./controllers/auth.controller.test');
  });
  describe('base.controller', () => {
    require('./controllers/base.controller.test');
  });
});
